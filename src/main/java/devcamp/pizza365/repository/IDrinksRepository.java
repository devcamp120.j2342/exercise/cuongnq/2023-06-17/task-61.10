package devcamp.pizza365.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import devcamp.pizza365.model.CDrinks;

public interface IDrinksRepository extends JpaRepository<CDrinks , Long> {
    
}
